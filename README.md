# Instructions and description

## Description
  - The software is part of a coding assignment from Accenture.
  - The software is mostly done with Node.js and express. It is then wrapped in a Docker volume.
  - The port it runs on locally is 4000
  - Pagination is implemented, and can be controlled with curser and limit. The response will also tell about the total length for all the places 
  that can be queried. 
  - The development enviroment also has unit tests, which can be used to test the functionality of the system with dummy inputs already inserted.
  - It also has a hot reload in docker container as well, so it works fine in the development mode as well.
  - Caching has been implemented with Redis, which is integrated with docker compose.


*--------------------------------------------------------------------------------------------*
## How to run and shutdown the API?

- The application is wrapped with Docker volume (so it can run on any OS). Please make sure you have docker installed. Here is the installation guide
https://docs.docker.com/get-docker/
- After installation, go to the directory of the API and write the command:
  ```
  docker-compose up --build -d

  ````
- To shutdown the api, in the same directory write
  ```
  docker-compose down

  ```
- To view the docker compose while it is running
  ```
  docker-compose ps -a
  
  ```

*--------------------------------------------------------------------------------------------*

## EndPoints

- /getPlaces?
  Which can take three paramter values:
    - cursor              //For the current pointer in the list
    - limit               // Number of pagination limit
    - tags                // filteration based on tags

*--------------------------------------------------------------------------------------------*
## Running Unit Test
- The unit test are written with Jest and SuperTester JS libraries. 
- To run the test first please make sure you have npm and node installed on your os. The installation guide can be found here https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

NOTE: When running the test in the dev mode without docker, it might need to have TypeScript installed globally or locally. Please check this:
https://code.visualstudio.com/docs/typescript/typescript-compiling

- Run the node moudule installation through the CLI on project directory
  ```
  npm install

  ```
- After making sure that the installation is complete, on the CLI run

 ```
 npm run test

 ```




*--------------------------------------------------------------------------------------------*

## TODO:
- Connect with Database
- Storing number of hits / view for the site
- access-control-allow-origin, only allowing specific IPs
- More unit test, those weren't done, due to limited time
- nginx, api gateway or another other method for load balancering


*--------------------------------------------------------------------------------------------*
