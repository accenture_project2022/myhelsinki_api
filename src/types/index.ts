export interface params<T> {
  params: T;
}

type StringORnull = string | null;

export interface HoursType {
  weekday_id: number,
  opens: StringORnull,
  closes: StringORnull,
  open24h: StringORnull;
}

export interface ResultantObject_old {
  id: string;
  name: {
    fi: StringORnull;
    en: StringORnull;
    sv: StringORnull;
    zh: StringORnull;
  },
  location: {
    lat: number | null,
    lon: number | null,
    address: {
      street_address: StringORnull,
      postal_code: StringORnull,
      locality: StringORnull,
      neighbourhood: StringORnull;
    };
  };
  info_url: StringORnull,
  opening_hours: {
    hours: HoursType[];
  };
  isOpen?: string;
  description: {
    intro?: string | null,
    body: string,
    images?: [{
      url: string;
      copyright_holder: string,
      license_type: {
        id: number,
        name: string,
      };
    }],
    media_id: string;
  };
}

export interface ResultantObject {
  id: string;
  opening_hours: {
    hours: HoursType[];
  };
  name: string;
  lat: number | null,
  lon: number | null,
  address: string;
  info_url: StringORnull,
  isOpen?: string;
  images?: string;
}