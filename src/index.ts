//Declaring the server
import { app } from './server';
import dotenv from 'dotenv';
import cors from 'cors';
//this file is for configuration
dotenv.config();

app.use(cors());

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});