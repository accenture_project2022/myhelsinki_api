import axios from 'axios';
import { HoursType } from '../types';
import { redisClient } from '../server';


export const httpRequestGET = (async <T extends {}>(endpoint: string, params?: T) => {
  const response = await axios({
    method: 'GET',
    url: endpoint,
    headers: { 'Accept': 'application/json' },
    params
  });
  return response.data;

});


export const parseData = <T>(data: any, params: string[]) => {
  const resultantData: T[] = [];

  data.map((item: any) => {
    let object: any = {};
    params.forEach(param => {
      switch (param) {
        case "name":
          try {
            object[param] = item[param]['en'] ? item[param]['en'] : "";
          }
          catch {
            object[param] = "";
          }
          break;
        case "location":
          try {
            object["lat"] = item["location"]['lat'] ? item["location"]['lat'] : "";
          }
          catch {
            object["lat"] = "";
          }
          try {
            object["lon"] = item["location"]['lon'] ? item["location"]['lon'] : "";
          }
          catch {
            object["lon"] = "";
          }
          break;
        case "address":
          try {
            object["address"] = item["location"]["address"]['street_address'] ? item["location"]["address"]['street_address'] : "";
          }
          catch {
            object["address"] = "";
          }
          break;
        case "images":
          try {
            object["images"] = item["description"]['images'] ? item["description"]['images'] : [];
          }
          catch {
            object["images"] = [];
          }
          break;
        case "meta":
          try {
            object["count"] = item["meta"]['count'] ? item["meta"]['count'] : "";
          }
          catch {
            object["count"] = "";
          }
          break;
        case "opening_hours":
          try {
            object["opening_hours"] = item["opening_hours"]['hours'] ? item["opening_hours"]['hours'] : [];
          }
          catch {
            object["opening_hours"] = [];
          }
        default:
          object[param] = item[param] ? item[param] : "";
          break;
      }

    });
    resultantData.push(object);
  });
  return resultantData;
};


export const isOpen = <T extends HoursType>(timedata: T[]) => {
  const getDayRN = new Date().getDay();
  const getTimeRN = new Date().getHours();

  for (let index = 0; index < timedata.length; index++) {
    const element = timedata[index];
    if (getDayRN === element.weekday_id && element.opens && element.closes) {
      const opening_hour = parseInt(element.opens?.split(":")[0]);
      const closing_hour = parseInt(element.closes?.split(":")[0]);

      return (getTimeRN >= opening_hour && getTimeRN < closing_hour) ? 'Open' : 'Closed';
    }
  };

  return "N/A";
};


export function securityHeader(res: any) {
  res.setHeader('object-src', 'self');
  res.setHeader('default-src', 'self');
  res.setHeader('script-src', 'self', 'unsafe-eval', 'unsafe-inline');
  res.setHeader('frame-src', 'self');
  res.setHeader('frame-ancestors', 'self');
  res.setHeader('form-action', 'self');
  res.setHeader('img-src', 'self');

  return res;
}


//Caching

export const cachingMiddlewear = (async (req: any, res: any, next: any) => {
  try {
    const data = await redisClient.get(req.url);
    if (data !== undefined && data !== null) {
      console.log('cache called', req.url);
      return res.send(JSON.parse(data));
    }
    else {
      next();
    }
  }
  catch (error) {
    console.log(error);
    next();
  }

});

