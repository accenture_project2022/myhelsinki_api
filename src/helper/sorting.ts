//Getting all the tags

export const tagExtractor = ((data: any) => {
  try {
    let tagList: any = [];
    data.forEach((item: any) => {
      item.tags.forEach((tag: any) => {
        tagList = [...tagList, tag];
      });
    });

    //Cleaning up and getting unique values
    const filteredList = ([... new Set(tagList.map((item: any) => item.name))]);

    //Making it sutaible for frontEnd State
    return filteredList.map((item: any) => {
      return { tag: item, value: 0 };
    });

  }
  catch (e) {
    console.log(e);
  }
});