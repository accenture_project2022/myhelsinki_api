import { Router } from 'express';
//helper
import { parseData, isOpen } from '../../helper';

//Calling the endpoint
import { myhelsinkiEndPoint, defaultExpiration } from '../../constants';
import { httpRequestGET } from './../../helper';

import { ResultantObject } from '../../types';
import { redisClient } from '../../server';



interface Params {
  start: number;
  limit: number;
  language_filter?: any;
  tags_filter?: any;
}


const router = Router();
router.get('/', async (req, res) => {
  //Calling the data for places
  try {
    const curser = typeof (req.query.curser) === "string" ? parseInt(req.query.curser) : typeof (req.query.curser) === "number" ? req.query.curser : 0;
    const limit = typeof (req.query.limit) === "string" ? parseInt(req.query.limit) : typeof (req.query.limit) === "number" ? req.query.limit : 5;
    //const start = curser === 0 ? 0 : (curser - 1) * limit;
    const start = curser === 0 ? 0 : curser + limit;
    const tags_filter = typeof (req.query.tags_filter) !== "undefined" ? req.query.tags_filter : undefined;


    const language_filter = req.query.language_filter ? req.query.language_filter : 'en';

    //according to the documentation
    const params = {
      start,
      limit,
      language_filter,
      tags_filter
    };

    //Getting the data
    const response = await httpRequestGET<Params>(myhelsinkiEndPoint + "v1/places/", params);

    //parsing the data for get request
    const neededParamters = ["id", "name", "location", "address", "info_url", "opening_hours", "images"];
    let parsedResponse = parseData<ResultantObject>(response.data, neededParamters);

    // isOpen for each place
    parsedResponse.map((element) => {
      try {
        element.isOpen = isOpen(element.opening_hours.hours);
      }
      catch {
        element.isOpen = 'N/A';
      }
    });

    //filtered for failed queries //Maybe Parse the reponse if error // Not part of the scope
    //parsedResponse = parsedResponse.filter((elemenet) => elemenet.isOpen !== 'N/A');

    const finalData = JSON.stringify({
      data: parsedResponse,
      count: response.meta.count
    });

    //catching the response for finalData in Redis
    await redisClient.setEx(req.url, defaultExpiration, finalData);

    res.send(finalData);
  }
  catch (error) {
    res.status(400);
    res.send("Querry Payload Incorrect");
  }

});

export { router };