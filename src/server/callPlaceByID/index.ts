import { Router } from 'express';
//helper
import { parseData, isOpen } from '../../helper';
import { redisClient } from '../../server';

//Calling the endpoint
import { myhelsinkiEndPoint, defaultExpiration } from '../../constants';
import { httpRequestGET } from './../../helper';
import { ResultantObject } from '../../types';


const router = Router();

router.get('/:id', async (req, res) => {
  try {
    //getting the ID
    const id = req.params.id;

    const response = await httpRequestGET(myhelsinkiEndPoint + `v1/place/${id}`);

    const neededParamters = ["id", "name", "location", "address", "info_url", "opening_hours", "images"];
    let parsedResponse = parseData<ResultantObject>([response], neededParamters);

    // isOpen for each place
    parsedResponse.map((element) => {
      try {
        element.isOpen = isOpen(element.opening_hours.hours);
      }
      catch {
        element.isOpen = 'N/A';
      }
    });

    const finalData = JSON.stringify({
      data: parsedResponse
    });

    //catching the response for finalData in Redis
    await redisClient.setEx(req.url, defaultExpiration, finalData);
    res.send(finalData);
  }
  catch {
    res.status(400);
    res.send("Querry Payload Incorrect");
  }
});

export { router };