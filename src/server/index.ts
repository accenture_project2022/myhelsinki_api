import express from 'express';
import cors from 'cors';
import { createClient } from 'redis';
import { cachingMiddlewear } from '../helper';

// importing routes
import { router as callPlaces } from './callPlaces';
import { router as callAllTags } from './callAllTags';
import { router as callPlaceByID } from './callPlaceByID';

export const redisClient = createClient({
  url: process.env.REDIS_URL
});
(async () => {
  redisClient.on('error', (err) => console.log('Redis Client Error', err));
  await redisClient.connect();
})();


const app = express();

app.use(express.urlencoded({ extended: true }));

app.use(express.json());

//using cors ONLY for development
app.use(cors());

//Now defining routes
app.use('/getPlaces', cachingMiddlewear, callPlaces);
app.use('/getAllTags', cachingMiddlewear, callAllTags);
app.use('/getPlace', cachingMiddlewear, callPlaceByID);


export { app };