import { response, Router } from 'express';
//helper
import { tagExtractor } from '../../helper/sorting';
import { redisClient } from '../../server';

//Calling the endpoint
import { myhelsinkiEndPoint, defaultExpiration } from '../../constants';
import { httpRequestGET } from './../../helper';

import { ResultantObject } from '../../types';


const router = Router();
router.get('/', async (req, res) => {

  try {

    //Calling the endpoint again
    const response = await httpRequestGET(myhelsinkiEndPoint + "v1/places/");

    //Tag extractor
    const tags = JSON.stringify(tagExtractor(response.data));
    //Caching Data
    await redisClient.setEx(req.url, defaultExpiration, tags);

    res.send(tags);

  }
  catch {
    res.status(400);
    res.send("Tags not found");
  }

});


export { router };