import request from 'supertest';
import { app } from './server/index';

describe('Checking All the routes', () => {
  it('GET /getPlaces ====> should return 200 and send data', () => {
    return request(app)
      .get('/getPlaces')
      .expect('Content-Type', /json/)
      .expect(200);
  });
  it('GET /getPlaces?curser=0&limit=50 ====> should return 200 and send data', () => {
    return request(app)
      .get('/getPlaces?curser=0&limit=50')
      .expect('Content-Type', /json/)
      .expect(200);
  });
  it('GET /getAllTags ====> should return 200 and send data', () => {
    return request(app)
      .get('/getAllTags')
      .expect('Content-Type', /json/)
      .expect(200);
  });
  it('GET /getPlace/3816 ====> should return 200 and send data', () => {
    return request(app)
      .get('/getPlace/3816')
      .expect('Content-Type', /json/)
      .expect(200);
  });

});
